q# Project 2 (P2) grading rubric

## Code reviews

- The Gradescope autograder will make deductions based on the rubric provided below.
- To ensure that you don't lose any points, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:
- Did not save the notebook file prior to running the cell containing "export". We cannot see your output if you do not save before generating the zip file. This deduction will become stricter for future projects. (-1)
- Used conditionals/loops or other material not covered in class yet. (-20)


### Question specific guidelines:

- q1 (5)

- q2 (5)
	- `grad_course_num` variable is hardcoded as `319` (-5)

- q3 (5)
	- `num_cartons` variable is hardcoded as `168` (-5)

- q4 (5)

- q5 (5)
	- `data_type` variable is hardcoded as `int` (-5)

- q6 (5)
	- `data_type` variable is hardcoded as `float` (-5)

- q7 (5)
	- `data_type` variable is hardcoded as `str` (-5)

- q8 (5)
	- `data_type` variable is hardcoded as `bool` (-5)

- q9 (5)
	- `data_type` variable is hardcoded as `str` (-5)

- q10 (5)
	- `data_type` variable is hardcoded as `bool` (-5)

- q11 (5)
	- `smileys` variable is hardcoded (-5)

- q12 (5)
	- `curr_year` variable is hardcoded as `"2023"` (-5)

- q13 (5)
	- `cube_volume` variable is hardcoded as `216` (-5)
	- expression to compute `cube_volume` is incorrect (-3)
	- `cube_side` variable is not used in `cube_volume` computation (-1)

- q14 (5)
	- `cylinder_volume` variable is hardcoded as `3400.62` (-5)
	- expression to compute `cylinder_volume` is incorrect (-3)
	- `cylinder_radius` variable is not defined, initialized, and used in `cube_volume` computation (-1)
	- `cylinder_height` variable is not defined, initialized, and used in `cube_volume` computation (-1)

- q15 (5)
	- `safe_operation` variable is hardcoded as `True` (-5)
	- expression to compute `safe_operation` is incorrect (-3)
	- `TRAILER_LIMIT` and / or `trailer_weight` values are modified (-1)
	- `equals` comparison is missing (-1)

- q16 (5)
	- `safe_operation` variable is hardcoded as `True` (-5)
	- expression to compute `safe_operation` is incorrect (-3)
	- `UPPER_LIMIT` and / or `LOWER_LIMIT` and / or `truck_weight` values are modified (-1)
	- `equals` comparison is missing (-1)

- q17 (5)
	- expression to compute `success` is changed (-5)
	- initialization value of more than one variable is changed (-3)

- q18 (5)
	- `success` variable is hardcoded as `False` (-5)
	- expression to compute `success` is not modified correctly (-3)
	- variables `short` and / or `dark` are initialized to different values (-1)

- q19 (5)
	- `primary_color` variable is hardcoded as `True` (-5)
	- expression to compute `primary_color` is incorrect (-3)
	- variable `color` is initialized to different value (-1)

- q20 (5)
	- `average_score` variable is hardcoded as `33.75` (-5)
	- expression to compute `average_score` is incorrect (-3)
	- `alice_score` variables is not defined, initialized, and used in `average_score` computation (-0.5)
	- `bob_score` variables is not defined, initialized, and used in `average_score` computation (-0.5)
	- `chang_score` variables is not defined, initialized, and used in `average_score` computation (-0.5)
	- `divya_score` variables is not defined, initialized, and used in `average_score` computation (-0.5)
