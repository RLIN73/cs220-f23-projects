# Project 2 (P2)

## Clarifications/Corrections:

* None yet.

**Find any issues?** Report to us:

- Divyam Anshumaan <anshumaan@wisc.edu>
- Abhinav Harish <aharish2@wisc.edu>


## Note on Academic Misconduct:
Starting from P2, you are **allowed** to work with a partner on your projects. While it is not required that you work with a partner, it is **recommended** that you find a project partner as soon as possible as the projects will get progressively harder. Be careful **not** to work with more than one partner. If you worked with a partner on Lab-P2, you are **not** allowed to finish your project with a different partner. You may either continue to work with the same partner, or work on P2 alone. Now may be a good time to review our [course policies](https://cs220.cs.wisc.edu/f23/syllabus.html).


## Instructions:

In this project, we will focus on types, operators, and boolean logic. To start, create a `p2` directory, and download `p2.ipynb` and `public_tests.py`. Make sure to follow the steps mentioned in [Lab-P2](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/lab-p2#task-21-download-boolipynb-opsipynb-and-modipynb) to download these files.

You will work on `p2.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**.

After you've downloaded the file to your `p2` directory, open a terminal window and use `cd` to navigate to that directory. To make sure you're in the correct directory in the terminal, type `pwd`. To make sure you've downloaded the notebook file, type `ls` to ensure that `p2.ipynb` is listed. Then run the command `jupyter notebook` to start Jupyter, and get started on the project!

**IMPORTANT**: You should **NOT** terminate/close the session where you run the above command. If you need to use any other Terminal/PowerShell commands, open a new window instead. Keep constantly saving your notebook file, by either clicking the "Save and Checkpoint" button (floppy disk) or using the appropriate keyboard shortcut.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f23-projects/-/tree/main/p2/rubric.md), to ensure that you don't lose points during code review.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P2 assignment.
- If you completed the project with a **partner**, make sure to **add their name** by clicking "Add Group Member"
in Gradescope when uploading the P2 zip file.

   <img src="images/add_group_member.png" width="400">

   **Warning:** You will have to add your partner on Gradescope even if you have filled out this information in your `p2.ipynb` notebook.

- It is **your responsibility** to make sure that your project clears auto-grader tests on the Gradescope test system. Otter test results should be available within forty minutes after your submission (usually within ten minutes). You should be able to see both PASS / FAIL results for the 20 test cases, which is accessible via Gradescope Dashboard (as in the image below):

    <img src="images/gradescope.png" width="400">

   You can view your **final score** at the end of the page.
